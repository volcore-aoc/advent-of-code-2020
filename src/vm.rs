use std::collections::HashSet;
use std::str::FromStr;
use std::string::ParseError;

#[derive(Debug, Clone)]
pub struct VM {
    pub code: Vec<Instruction>,
    pub ip: i64,
    pub acc: i64,
}

#[derive(Debug, Clone)]
pub enum Instruction {
    Nop(i64),
    Acc(i64),
    Jmp(i64),
    EOF(),
}

impl VM {
    pub fn reset(&mut self) {
        self.ip = 0;
        self.acc = 0;
    }

    pub fn dump(&self) {
        println!("{:?}", self);
    }

    pub fn is_done(&self) -> bool {
        return self.ip < 0 || self.ip >= (self.code.len() as i64);
    }

    pub fn get_code(&self, idx: i64) -> &Instruction {
        self.code.get(idx as usize).unwrap_or(&Instruction::EOF())
    }

    pub fn set_code(&mut self, idx: i64, inst: &Instruction) {
        self.code[idx as usize] = inst.clone()
    }

    pub fn step(&mut self) {
        if self.is_done() {
            return;
        }
        let inst = &self.code[self.ip as usize];
        match inst {
            Instruction::Nop(_) => {
                self.ip += 1;
            }
            Instruction::Jmp(x) => {
                self.ip += x;
            }
            Instruction::Acc(x) => {
                self.acc += x;
                self.ip += 1;
            }
            Instruction::EOF() => {}
        }
    }

    // Might have to change this for later days
    pub fn does_terminate(&mut self) -> bool {
        let mut tags: HashSet<i64> = HashSet::new();
        while !self.is_done() {
            if tags.contains(&self.ip) {
                return false;
            }
            tags.insert(self.ip);
            self.step();
        }
        true
    }
}

impl FromStr for VM {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let code = s
            .lines()
            .map(|s| {
                let parts: Vec<&str> = s.split(' ').collect();
                let op1 = parts.get(1).and_then(|s| s.parse::<i64>().ok());
                match parts[0] {
                    "nop" => Some(Instruction::Nop(op1.unwrap_or(0))),
                    "jmp" => Some(Instruction::Jmp(op1.unwrap_or(0))),
                    "acc" => Some(Instruction::Acc(op1.unwrap_or(0))),
                    _ => None,
                }
            })
            .filter_map(|x| x)
            .collect();
        Ok(VM {
            acc: 0,
            ip: 0,
            code: code,
        })
    }
}
