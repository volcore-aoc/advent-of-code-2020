pub mod math;
pub mod vm;

use std::fs;

pub fn parse_int_list(s: &str) -> Vec<i64> {
    let split = s.split_ascii_whitespace();
    let parsed = split.map(|line| line.parse::<i64>().unwrap());
    parsed.collect::<Vec<i64>>()
}

pub fn parse_int_list_ext(s: &str, sep: char) -> Vec<i64> {
    let split = s.split(sep);
    let parsed = split.map(|line| line.parse::<i64>().unwrap());
    parsed.collect::<Vec<i64>>()
}

#[test]
fn test_parse_int_list() {
    let sample_input = "1721\n979\n366\n299\n675\n1456";
    let list = parse_int_list(sample_input);
    assert_eq!(list, [1721, 979, 366, 299, 675, 1456]);
}

pub fn load_input(day: i32) -> String {
    let filename = format!("input/input{:02}.txt", day);
    let result = fs::read_to_string(&filename);
    if !result.is_ok() {
        println!("Error: Couldn't find input {}", filename);
        return String::from("");
    }
    return result.ok().unwrap();
}
