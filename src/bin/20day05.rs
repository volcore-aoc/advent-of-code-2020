use advent_of_code_2020::load_input;

fn solve_a(input: &str) -> i64 {
    input.lines().map(parse_pass).max().unwrap_or(0)
}

fn solve_b(input: &str) -> i64 {
    let mut int_list = input.lines().map(parse_pass).collect::<Vec<i64>>();
    int_list.sort();
    for x in 1..int_list.len() {
        if int_list[x - 1] + 2 == int_list[x] {
            return int_list[x - 1] + 1;
        }
    }
    0
}

fn parse_pass(s: &str) -> i64 {
    let bs = s
        .replace('B', "1")
        .replace('F', "0")
        .replace('R', "1")
        .replace('L', "0");
    isize::from_str_radix(bs.as_str(), 2).unwrap() as i64
}

fn main() {
    println!("Day05a: {}", solve_a(load_input(5).as_str()));
    println!("Day05b: {}", solve_b(load_input(5).as_str()));
}

#[test]
fn test() {
    assert_eq!(parse_pass("BFFFBBFRRR"), 567);
    assert_eq!(parse_pass("FFFBBBFRRR"), 119);
    assert_eq!(parse_pass("BBFFBBFRLL"), 820);
    assert_eq!(solve_a("BFFFBBFRRR\nFFFBBBFRRR\nBBFFBBFRLL"), 820);
}
