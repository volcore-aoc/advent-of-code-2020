use advent_of_code_2020::load_input;
use advent_of_code_2020::math::Vec2i64;
use core::fmt;
use std::collections::HashMap;

#[derive(Debug)]
struct HyperContext {
    cubes: HashMap<i64, Context>,
}

#[derive(Debug)]
struct Context {
    slices: HashMap<i64, Slice>,
}

#[derive(Debug)]
struct Slice {
    rows: HashMap<i64, Row>,
}

#[derive(Debug)]
struct Row {
    cells: HashMap<i64, char>,
}

impl HyperContext {
    pub fn empty() -> HyperContext {
        HyperContext {
            cubes: HashMap::new(),
        }
    }

    pub fn from_str(s: &str) -> HyperContext {
        let mut cubes = HashMap::new();
        let cube = Context::from_str(s);
        cubes.insert(0, cube);
        HyperContext { cubes }
    }

    pub fn get(&self, x: i64, y: i64, z: i64, w: i64) -> char {
        self.cubes
            .get(&w)
            .and_then(|s| Some(s.get(x, y, z)))
            .unwrap_or('.')
    }

    pub fn dim_x(&self) -> Vec2i64 {
        self.cubes.iter().fold(Vec2i64::empty_bounds(), |b, x| {
            x.1.dim_x().extend_bounds(&b)
        })
    }

    pub fn dim_y(&self) -> Vec2i64 {
        self.cubes.iter().fold(Vec2i64::empty_bounds(), |b, x| {
            x.1.dim_y().extend_bounds(&b)
        })
    }

    pub fn dim_z(&self) -> Vec2i64 {
        self.cubes.iter().fold(Vec2i64::empty_bounds(), |b, x| {
            x.1.dim_z().extend_bounds(&b)
        })
    }

    pub fn dim_w(&self) -> Vec2i64 {
        Vec2i64 {
            x: *self.cubes.keys().min().unwrap_or(&0),
            y: *self.cubes.keys().max().unwrap_or(&0),
        }
    }

    pub fn set(&mut self, x: i64, y: i64, z: i64, w: i64, c: char) {
        self.cubes
            .entry(w)
            .or_insert_with(Context::empty)
            .set(x, y, z, c);
    }

    pub fn count(&self) -> usize {
        self.cubes.iter().map(|r| r.1.count()).sum()
    }
}

impl Context {
    pub fn empty() -> Context {
        Context {
            slices: HashMap::new(),
        }
    }

    pub fn from_str(s: &str) -> Context {
        let mut slices = HashMap::new();
        let slice = Slice::from_str(s);
        slices.insert(0, slice);
        Context { slices }
    }

    pub fn get(&self, x: i64, y: i64, z: i64) -> char {
        self.slices
            .get(&z)
            .and_then(|s| Some(s.get(x, y)))
            .unwrap_or('.')
    }

    pub fn dim_x(&self) -> Vec2i64 {
        self.slices.iter().fold(Vec2i64::empty_bounds(), |b, x| {
            x.1.dim_x().extend_bounds(&b)
        })
    }

    pub fn dim_y(&self) -> Vec2i64 {
        self.slices.iter().fold(Vec2i64::empty_bounds(), |b, x| {
            x.1.dim_y().extend_bounds(&b)
        })
    }

    pub fn dim_z(&self) -> Vec2i64 {
        Vec2i64 {
            x: *self.slices.keys().min().unwrap_or(&0),
            y: *self.slices.keys().max().unwrap_or(&0),
        }
    }

    pub fn set(&mut self, x: i64, y: i64, z: i64, c: char) {
        self.slices
            .entry(z)
            .or_insert_with(Slice::empty)
            .set(x, y, c);
    }

    pub fn count(&self) -> usize {
        self.slices.iter().map(|r| r.1.count()).sum()
    }
}

impl Slice {
    pub fn empty() -> Slice {
        Slice {
            rows: HashMap::new(),
        }
    }

    pub fn from_str(s: &str) -> Slice {
        Slice {
            rows: s
                .lines()
                .enumerate()
                .map(|x| (x.0 as i64, Row::from_str(x.1)))
                .collect(),
        }
    }

    pub fn get(&self, x: i64, y: i64) -> char {
        self.rows
            .get(&y)
            .and_then(|r| Some(r.get(x)))
            .unwrap_or('.')
    }

    pub fn dim_x(&self) -> Vec2i64 {
        self.rows.iter().fold(Vec2i64::empty_bounds(), |b, x| {
            x.1.dim_x().extend_bounds(&b)
        })
    }

    pub fn dim_y(&self) -> Vec2i64 {
        Vec2i64 {
            x: *self.rows.keys().min().unwrap(),
            y: *self.rows.keys().max().unwrap(),
        }
    }

    pub fn set(&mut self, x: i64, y: i64, c: char) {
        self.rows.entry(y).or_insert_with(Row::empty).set(x, c);
    }

    pub fn count(&self) -> usize {
        self.rows.iter().map(|r| r.1.count()).sum()
    }
}

impl Row {
    pub fn empty() -> Row {
        Row {
            cells: HashMap::new(),
        }
    }

    pub fn from_str(s: &str) -> Row {
        let cells = s
            .chars()
            .enumerate()
            .filter(|x| x.1 == '#')
            .map(|x| (x.0 as i64, x.1))
            .collect();
        Row { cells }
    }

    pub fn get(&self, x: i64) -> char {
        *self.cells.get(&x).unwrap_or(&'.')
    }

    pub fn dim_x(&self) -> Vec2i64 {
        Vec2i64 {
            x: *self.cells.keys().min().unwrap(),
            y: *self.cells.keys().max().unwrap(),
        }
    }

    pub fn set(&mut self, x: i64, c: char) {
        self.cells.insert(x, c);
    }

    pub fn count(&self) -> usize {
        self.cells.len()
    }
}

impl fmt::Display for HyperContext {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let dim_w = self.dim_w();
        let dim_z = self.dim_z();
        let dim_y = self.dim_y();
        let dim_x = self.dim_x();
        let mut s = String::new();
        s += format!(
            "x={:?} y={:?} z={:?} w={:?} count={}\n",
            dim_x,
            dim_y,
            dim_z,
            dim_w,
            self.count()
        )
        .as_str();
        for w in dim_w.x..=dim_w.y {
            for z in dim_z.x..=dim_z.y {
                s += format!("z={}, w={}\n", z, w).as_str();
                for y in dim_y.x..=dim_y.y {
                    for x in dim_x.x..=dim_x.y {
                        s.push(self.get(x, y, z, w));
                    }
                    s.push('\n');
                }
            }
        }
        f.write_str(s.as_str())
    }
}

impl fmt::Display for Context {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let dim_z = self.dim_z();
        let dim_y = self.dim_y();
        let dim_x = self.dim_x();
        let mut s = String::new();
        s += format!(
            "x={:?} y={:?} z={:?} count={}\n",
            dim_x,
            dim_y,
            dim_z,
            self.count()
        )
        .as_str();
        for z in dim_z.x..=dim_z.y {
            s += format!("z={}\n", z).as_str();
            for y in dim_y.x..=dim_y.y {
                for x in dim_x.x..=dim_x.y {
                    s.push(self.get(x, y, z));
                }
                s.push('\n');
            }
        }
        f.write_str(s.as_str())
    }
}

fn solve_a(input: &str) -> usize {
    let mut ctx = Context::from_str(input);
    for _ in 0..6 {
        println!("{}", ctx);
        let dim_z = ctx.dim_z();
        let dim_y = ctx.dim_y();
        let dim_x = ctx.dim_x();
        let mut new_ctx = Context::empty();
        for z in dim_z.x - 1..=dim_z.y + 1 {
            for y in dim_y.x - 1..=dim_y.y + 1 {
                for x in dim_x.x - 1..=dim_x.y + 1 {
                    // Read the neighbourhood from the old context
                    let mut count = 0usize;
                    for dz in -1i64..=1i64 {
                        for dy in -1i64..=1i64 {
                            for dx in -1i64..=1i64 {
                                if dx == 0 && dy == 0 && dz == 0 {
                                    continue;
                                }
                                if ctx.get(x + dx, y + dy, z + dz) == '#' {
                                    count += 1
                                }
                            }
                        }
                    }
                    let current = ctx.get(x, y, z);
                    match current {
                        '#' => {
                            if count == 2 || count == 3 {
                                new_ctx.set(x, y, z, '#')
                            }
                        }
                        _ => {
                            if count == 3 {
                                new_ctx.set(x, y, z, '#')
                            }
                        }
                    }
                }
            }
        }
        ctx = new_ctx;
    }
    println!("{}", ctx);
    ctx.count()
}

fn solve_b(input: &str) -> usize {
    let mut ctx = HyperContext::from_str(input);
    for _ in 0..6 {
        let dim_w = ctx.dim_w();
        let dim_z = ctx.dim_z();
        let dim_y = ctx.dim_y();
        let dim_x = ctx.dim_x();
        let mut new_ctx = HyperContext::empty();
        println!("{}", ctx);
        for w in dim_w.x - 1..=dim_w.y + 1 {
            for z in dim_z.x - 1..=dim_z.y + 1 {
                for y in dim_y.x - 1..=dim_y.y + 1 {
                    for x in dim_x.x - 1..=dim_x.y + 1 {
                        // Read the neighbourhood from the old context
                        let mut count = 0usize;
                        for dw in -1i64..=1i64 {
                            for dz in -1i64..=1i64 {
                                for dy in -1i64..=1i64 {
                                    for dx in -1i64..=1i64 {
                                        if dx == 0 && dy == 0 && dz == 0 && dw == 0 {
                                            continue;
                                        }
                                        if ctx.get(x + dx, y + dy, z + dz, w + dw) == '#' {
                                            count += 1
                                        }
                                    }
                                }
                            }
                        }
                        let current = ctx.get(x, y, z, w);
                        match current {
                            '#' => {
                                if count == 2 || count == 3 {
                                    new_ctx.set(x, y, z, w, '#')
                                }
                            }
                            _ => {
                                if count == 3 {
                                    new_ctx.set(x, y, z, w, '#')
                                }
                            }
                        }
                    }
                }
            }
        }
        ctx = new_ctx;
    }
    ctx.count()
}

fn main() {
    let day = 17;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input1 = ".#.\n..#\n###";
    assert_eq!(solve_a(input1), 112);
    assert_eq!(solve_b(input1), 848);
}
