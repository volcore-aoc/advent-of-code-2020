use advent_of_code_2020::load_input;

peg::parser! {
  grammar formula_parser() for str {
    rule number() -> i64
      = n:$(['0'..='9']+) { n.parse().unwrap() }

    pub rule arithmetic() -> i64 = precedence!{
      x:(@) " + " y:@ { x + y }
      x:(@) " * " y:@ { x * y }
      --
      n:number() { n }
      "(" e:arithmetic() ")" { e }
    }

    pub rule arithmetic_b() -> i64 = precedence!{
      x:(@) " * " y:@ { x * y }
      --
      x:(@) " + " y:@ { x + y }
      --
      n:number() { n }
      "(" e:arithmetic_b() ")" { e }
    }
  }
}

fn solve_a(s: &str) -> i64 {
    s.lines().flat_map(formula_parser::arithmetic).sum()
}

fn solve_b(s: &str) -> i64 {
    s.lines().flat_map(formula_parser::arithmetic_b).sum()
}

fn main() {
    let day = 18;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    assert_eq!(solve_a("2 * 3 + (4 * 5)"), 26);
    assert_eq!(solve_a("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 437);
    assert_eq!(solve_a("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"), 12240);
    assert_eq!(
        solve_a("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"),
        13632
    );
    assert_eq!(solve_b("1 + (2 * 3) + (4 * (5 + 6))"), 51);
    assert_eq!(solve_b("2 * 3 + (4 * 5)"), 46);
    assert_eq!(solve_b("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 1445);
    assert_eq!(solve_b("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"), 669060);
    assert_eq!(
        solve_b("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"),
        23340
    );
}
