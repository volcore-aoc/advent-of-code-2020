use advent_of_code_2020::load_input;
use advent_of_code_2020::math::Vec2i64;
use std::str::FromStr;
use std::string::ParseError;

#[derive(Debug)]
struct Context {
    grid: Vec<String>,
    w: usize,
    h: usize,
    mode_b: bool,
}

impl FromStr for Context {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let grid: Vec<String> = s.lines().map(String::from).collect();
        let w = grid[0].len();
        let h = grid.len();
        Ok(Context {
            grid,
            w,
            h,
            mode_b: false,
        })
    }
}

impl Context {
    fn read(&self, x: i64, y: i64) -> char {
        self.grid
            .get(y as usize)
            .and_then(|s| s.chars().nth(x as usize))
            .unwrap_or('.')
    }

    fn trace_occupied(&self, x: i64, dx: i64, y: i64, dy: i64) -> bool {
        let mut xy = Vec2i64 { x, y };
        let dxy = Vec2i64 { x: dx, y: dy };
        let min = Vec2i64 { x: 0, y: 0 };
        let max = Vec2i64 {
            x: self.w as i64,
            y: self.h as i64,
        };
        loop {
            xy.add(&dxy);
            if !xy.inside_bounds(&min, &max) {
                return false;
            }
            match self.read(xy.x, xy.y) {
                // Hit an occupied chair
                '#' => return true,
                // Hit an empty chair, so not occupied
                'L' => return false,
                _ => continue,
            }
        }
    }

    fn count_occupied(&self, x: i64, y: i64) -> usize {
        // All neighbouring offsets
        let offsets: [(i64, i64); 8] = [
            (-1, -1),
            (0, -1),
            (1, -1),
            (-1, 0),
            (1, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        ];
        if self.mode_b {
            offsets
                .iter()
                .filter(|(dx, dy)| self.trace_occupied(x, *dx, y, *dy))
                .count()
        } else {
            offsets
                .iter()
                .filter(|(dx, dy)| self.read(x + dx, y + dy) == '#')
                .count()
        }
    }

    fn step(&mut self) {
        let mut new_grid = Vec::new();
        for y in 0..self.h {
            let mut s = String::from("");
            for x in 0..self.w {
                // Skip floor
                match self.read(x as i64, y as i64) {
                    '#' => {
                        if self.count_occupied(x as i64, y as i64)
                            >= (if self.mode_b { 5 } else { 4 })
                        {
                            s.push('L')
                        } else {
                            s.push('#')
                        }
                    }
                    'L' => {
                        if self.count_occupied(x as i64, y as i64) == 0 {
                            s.push('#')
                        } else {
                            s.push('L')
                        }
                    }
                    _ => s.push('.'),
                }
            }
            new_grid.push(s);
        }
        self.grid = new_grid;
    }

    fn step_to_converge(&mut self) {
        let mut num_iterations = 0;
        let mut pre = self.grid.join("\n");
        loop {
            num_iterations += 1;
            self.step();
            let post = self.grid.join("\n");
            if post == pre {
                break;
            }
            pre = post;
        }
        println!("Converged after {:?} iterations...", num_iterations);
    }

    fn count_total_occupied(&self) -> usize {
        self.grid
            .iter()
            .map(|s| s.chars().filter(|c| *c == '#').count())
            .sum()
    }
}

fn solve_a(input: &str) -> usize {
    let mut ctx = Context::from_str(input).expect("failed to load input");
    ctx.step_to_converge();
    ctx.count_total_occupied()
}

fn solve_b(input: &str) -> usize {
    let mut ctx = Context::from_str(input).expect("failed to load input");
    ctx.mode_b = true;
    ctx.step_to_converge();
    ctx.count_total_occupied()
}

fn main() {
    let day = 11;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input1 = "L.LL.LL.LL\nLLLLLLL.LL\nL.L.L..L..\nLLLL.LL.LL\nL.LL.LL.LL\nL.LLLLL.LL\n..L.L.....\nLLLLLLLLLL\nL.LLLLLL.L\nL.LLLLL.LL";
    assert_eq!(solve_a(input1), 37);
    assert_eq!(solve_b(input1), 26);
}
