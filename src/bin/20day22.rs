use advent_of_code_2020::load_input;
use std::collections::VecDeque;

#[derive(Debug)]
struct Context {
    players: Vec<Player>,
}

#[derive(Debug)]
struct Player {
    deck: VecDeque<i64>,
}

fn parse(s: &str) -> Context {
    let players = s
        .split("\n\n")
        .map(|x| {
            let deck = x
                .lines()
                .skip(1)
                .map(|s| s.parse::<i64>().unwrap())
                .collect();
            Player { deck }
        })
        .collect();
    Context { players }
}

impl Context {
    fn done(&self) -> bool {
        self.players.iter().any(|p| p.deck.len() == 0)
    }
}

fn solve_a(s: &str) -> i64 {
    let mut ctx = parse(s);
    println!("{:?}", ctx);
    let mut round = 1;
    while !ctx.done() {
        println!("-- Round {} --", round);
        println!("Player 1's deck: {:?}", ctx.players[0].deck);
        println!("Player 2's deck: {:?}", ctx.players[1].deck);
        println!("Player 1 plays: {}", ctx.players[0].deck[0]);
        println!("Player 2 plays: {}", ctx.players[1].deck[0]);
        let p1 = ctx.players[0].deck.pop_front().unwrap();
        let p2 = ctx.players[1].deck.pop_front().unwrap();
        if p1 > p2 {
            println!("Player 1 wins the round!");
            ctx.players[0].deck.push_back(p1);
            ctx.players[0].deck.push_back(p2);
        } else {
            println!("Player 2 wins the round!");
            ctx.players[1].deck.push_back(p2);
            ctx.players[1].deck.push_back(p1);
        }
        println!();
        round += 1;
    }
    println!("== Post-game results ==");
    println!("Player 1's deck: {:?}", ctx.players[0].deck);
    println!("Player 2's deck: {:?}", ctx.players[1].deck);
    let deck = if ctx.players[0].deck.is_empty() {
        &ctx.players[1].deck
    } else {
        &ctx.players[0].deck
    };
    deck.iter()
        .rev()
        .enumerate()
        .map(|x| (x.0 + 1) as i64 * x.1)
        .sum()
}

fn solve_b(s: &str) -> usize {
    let ctx = parse(s);
    0
}

fn main() {
    let day = 22;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    // println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input = "Player 1:\n9\n2\n6\n3\n1\n\nPlayer 2:\n5\n8\n4\n7\n10";
    assert_eq!(solve_a(input), 306);
    // assert_eq!(solve_b(input), "mxmxvkd,sqjhc,fvjkl");
}
