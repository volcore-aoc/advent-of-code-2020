use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;

use advent_of_code_2020::load_input;

#[derive(Debug)]
struct Context {
    dishes: Vec<Dish>,
}

#[derive(Debug)]
struct Dish {
    ingredients: Vec<String>,
    allergens: Vec<String>,
}

fn parse(s: &str) -> Context {
    let mut dishes: Vec<Dish> = vec![];
    for line in s.lines() {
        let parts = line.split('(').collect::<Vec<&str>>();
        let ingredients = parts[0]
            .trim()
            .split_ascii_whitespace()
            .map(|s| s.trim().to_string())
            .collect();
        let allergens = parts[1]
            .trim_start_matches("contains ")
            .trim_end_matches(")")
            .trim()
            .split(',')
            .map(|s| s.trim().to_string())
            .collect();
        dishes.push(Dish {
            allergens,
            ingredients,
        })
    }
    Context { dishes }
}

fn solve_a(s: &str) -> usize {
    let ctx = parse(s);
    // Build a map of the allergens
    let mut allergens: HashMap<String, HashSet<String>> = HashMap::new();
    for dish in ctx.dishes.iter() {
        for allergen in dish.allergens.iter() {
            // Get the current list
            let mut ingredients: HashSet<String> =
                HashSet::from_iter(dish.ingredients.iter().map(|s| s.to_owned()));
            // intersect with existing
            let existing = allergens.get(allergen);
            if existing.is_some() {
                ingredients = HashSet::from_iter(
                    ingredients
                        .intersection(existing.unwrap())
                        .map(|s| s.to_owned()),
                );
            }
            // Save
            allergens.insert(allergen.to_owned(), ingredients);
        }
    }
    // Find unique mappings
    let mut found = true;
    while found {
        // go through all allergens that have only one association, and remove them from all others
        for (allergen, set) in allergens.clone() {
            if set.len() == 1 {
                allergens.iter_mut().for_each(|(k, v)| {
                    if !k.eq(&allergen) {
                        v.remove(set.iter().nth(0).unwrap());
                    }
                });
            }
        }
        found = allergens.iter().any(|(_, v)| v.len() > 1);
    }
    // Create the hashset of allergic ingredients
    let allergic_ingredients: HashSet<String> = HashSet::from_iter(
        allergens
            .iter()
            .map(|(_, v)| v.iter().next().unwrap().to_owned()),
    );
    // Now, in every dish count all non-allergic ingredients
    let mut sum = 0;
    for dish in ctx.dishes.iter() {
        sum += dish
            .ingredients
            .iter()
            .filter(|i| !allergic_ingredients.contains(*i))
            .count();
    }
    sum
}

fn solve_b(s: &str) -> String {
    let ctx = parse(s);
    // Build a map of the allergens
    let mut allergens: HashMap<String, HashSet<String>> = HashMap::new();
    for dish in ctx.dishes.iter() {
        for allergen in dish.allergens.iter() {
            // Get the current list
            let mut ingredients: HashSet<String> =
                HashSet::from_iter(dish.ingredients.iter().map(|s| s.to_owned()));
            // intersect with existing
            let existing = allergens.get(allergen);
            if existing.is_some() {
                ingredients = HashSet::from_iter(
                    ingredients
                        .intersection(existing.unwrap())
                        .map(|s| s.to_owned()),
                );
            }
            // Save
            allergens.insert(allergen.to_owned(), ingredients);
        }
    }
    // Find unique mappings
    let mut found = true;
    while found {
        // go through all allergens that have only one association, and remove them from all others
        for (allergen, set) in allergens.clone() {
            if set.len() == 1 {
                allergens.iter_mut().for_each(|(k, v)| {
                    if !k.eq(&allergen) {
                        v.remove(set.iter().nth(0).unwrap());
                    }
                });
            }
        }
        found = allergens.iter().any(|(_, v)| v.len() > 1);
    }
    // Now sort alphabetically
    let mut x: Vec<(&String, &String)> = allergens
        .iter()
        .map(|(k, v)| (k, v.iter().next().unwrap()))
        .collect();
    x.sort();
    let y: Vec<String> = x.iter().map(|(_, v)| (*v).to_owned()).collect();
    y.join(",")
}

fn main() {
    let day = 21;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input = "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)\ntrh fvjkl sbzzf mxmxvkd (contains dairy)\nsqjhc fvjkl (contains soy)\nsqjhc mxmxvkd sbzzf (contains fish)";
    assert_eq!(solve_a(input), 5);
    assert_eq!(solve_b(input), "mxmxvkd,sqjhc,fvjkl");
}
