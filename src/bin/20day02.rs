use advent_of_code_2020::load_input;
use lazy_static::lazy_static;
use regex::Regex;

fn main() {
    println!("Day02a: {}", day02a(load_input(2).as_str()));
    println!("Day02b: {}", day02b(load_input(2).as_str()));
}

struct Day02Input {
    from: i64,
    to: i64,
    char: char,
    string: String,
}

fn parse_day02_input(input: &str) -> Vec<Day02Input> {
    lazy_static! {
        static ref RE: Regex =
            Regex::new(r"(?m)^(\d+)-(\d+)\s+(\w):\s+(\w+)$").expect("gobbled up the regex");
    }
    let mut list = Vec::new();
    for cap in RE.captures_iter(input) {
        list.push(Day02Input {
            from: cap[1].parse::<i64>().expect("can't parse from"),
            to: cap[2].parse::<i64>().expect("can't parse to"),
            char: cap[3].chars().next().expect("can't parse char"),
            string: String::from(&cap[4]),
        });
    }
    list
}

fn day02a(input: &str) -> i64 {
    let list = parse_day02_input(input);
    list.iter()
        .filter(|i| {
            let count = i.string.matches(i.char).count() as i64;
            count >= i.from && count <= i.to
        })
        .count() as i64
}

fn day02b(input: &str) -> i64 {
    let list = parse_day02_input(input);
    list.iter()
        .filter(|i| {
            let a = i
                .string
                .chars()
                .nth((i.from - 1) as usize)
                .expect("invalid string index in 'from'");
            let b = i
                .string
                .chars()
                .nth((i.to - 1) as usize)
                .expect("invalid string index in 'to'");
            (a == i.char) ^ (b == i.char)
        })
        .count() as i64
}

#[test]
fn test20day02() {
    let sample_input = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc";
    let result_a = day02a(sample_input);
    assert_eq!(result_a, 2);
    let result_b = day02b(sample_input);
    assert_eq!(result_b, 1);
}
