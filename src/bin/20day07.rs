use std::collections::HashSet;
use std::str::FromStr;
use std::string::ParseError;

use regex::Regex;

use advent_of_code_2020::load_input;

#[derive(Debug)]
struct Context {
    bags: Vec<Bag>,
}

#[derive(Debug)]
struct Bag {
    name: String,
    contains: Vec<(i64, String)>,
}

impl Context {
    fn find_containers(&self, name: &str) -> HashSet<String> {
        self.bags
            .iter()
            .filter(|b| b.contains.iter().filter(|o| o.1 == name).count() > 0)
            .fold(HashSet::new(), |mut h, x| {
                h.insert(x.name.to_owned());
                let sub = self.find_containers(x.name.as_str());
                h.extend(sub);
                h
            })
    }

    fn count_bags(&self, name: &str) -> i64 {
        self.bags
            .iter()
            // Find the bag of the right name
            .filter(|b| b.name == name)
            .map(|b| {
                // Go through all bags it contains, and count the number of bags inside, recursively
                b.contains
                    .iter()
                    .map(|t| t.0 * self.count_bags(t.1.as_ref()))
                    .sum::<i64>()
                    // + 1 here as we need to count the bag itself as well
                    + 1
            })
            .sum()
    }
}

impl FromStr for Bag {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let name_re = Regex::new(r"^(.*) bags contain.*$").expect("screwed up the regex");
        let name = name_re
            .captures(s)
            .expect("unable to get bag name via regex")[1]
            .to_owned();
        let bag_re = Regex::new(r"(\d+)\s+(.*?)\s+bag").expect("screwed up the regex");
        let contains = bag_re
            .captures_iter(s)
            .map(|b| {
                (
                    b[1].parse::<i64>().expect("invalid bag count"),
                    b[2].to_owned(),
                )
            })
            .collect();
        Ok(Bag { name, contains })
    }
}

impl FromStr for Context {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let bags = s
            .lines()
            .map(|s| Bag::from_str(s).expect("Failed to parse bag"))
            .collect();
        Ok(Context { bags })
    }
}

fn solve_a(input: &str) -> usize {
    Context::from_str(input)
        .expect("Failed to parse input")
        .find_containers("shiny gold")
        .len()
}

fn solve_b(input: &str) -> i64 {
    Context::from_str(input)
        .expect("Failed to parse input")
        .count_bags("shiny gold")
        // -1 here as we're excluding the shiny gold bag itself
        - 1
}

fn main() {
    println!("Day07a: {}", solve_a(load_input(7).as_str()));
    println!("Day07b: {}", solve_b(load_input(7).as_str()));
}

#[test]
fn test() {
    let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.\ndark orange bags contain 3 bright white bags, 4 muted yellow bags.\nbright white bags contain 1 shiny gold bag.\nmuted yellow bags contain 2 shiny gold bags, 9 faded blue bags.\nshiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.\ndark olive bags contain 3 faded blue bags, 4 dotted black bags.\nvibrant plum bags contain 5 faded blue bags, 6 dotted black bags.\nfaded blue bags contain no other bags.\ndotted black bags contain no other bags.";
    assert_eq!(solve_a(input), 4);
    assert_eq!(solve_b(input), 32);
    let input_b = "shiny gold bags contain 2 dark red bags.\ndark red bags contain 2 dark orange bags.\ndark orange bags contain 2 dark yellow bags.\ndark yellow bags contain 2 dark green bags.\ndark green bags contain 2 dark blue bags.\ndark blue bags contain 2 dark violet bags.\ndark violet bags contain no other bags.";
    assert_eq!(solve_b(input_b), 126);
}
