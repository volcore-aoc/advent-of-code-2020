use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::str::FromStr;
use std::string::ParseError;

use advent_of_code_2020::load_input;

#[derive(Debug)]
struct Context {
    passports: Vec<Passport>,
}

#[derive(Debug)]
struct Passport {
    ecl: Option<String>,
    pid: Option<String>,
    eyr: Option<String>,
    hcl: Option<String>,
    byr: Option<String>,
    iyr: Option<String>,
    cid: Option<String>,
    hgt: Option<String>,
}

fn year_in_range(s: &Option<String>, min: i32, max: i32) -> bool {
    if s.is_none() {
        return false;
    }
    let str = s.as_ref().unwrap();
    let parsed = str.parse::<i32>().ok();
    str.len() == 4 && parsed.is_some() && parsed.unwrap() >= min && parsed.unwrap() <= max
}

fn int_of_length(s: &Option<String>, len: usize) -> bool {
    if s.is_none() {
        return false;
    }
    let str = s.as_ref().unwrap();
    let parsed = str.parse::<i64>().ok();
    str.len() == len && parsed.is_some()
}

fn str_in_set(s: &Option<String>, set: Vec<&str>) -> bool {
    if s.is_none() {
        return false;
    }
    set.contains(&(s.as_ref().unwrap().as_str()))
}

fn hex_code(s: &Option<String>) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^#[[:xdigit:]]{6}$").expect("gobbled up the regex");
    }
    s.is_some() && RE.is_match(s.as_ref().unwrap())
}

fn valid_hgt(s: &Option<String>) -> bool {
    if s.is_none() {
        return false;
    }
    let str = s.as_ref().unwrap();
    let value_opt = str[..str.len() - 2].parse::<i64>().ok();
    if value_opt.is_none() {
        return false;
    }
    let value = value_opt.unwrap();
    if str.ends_with("cm") {
        value >= 150 && value <= 193
    } else if str.ends_with("in") {
        value >= 59 && value <= 76
    } else {
        false
    }
}

impl Passport {
    fn is_valid(&self) -> bool {
        self.ecl.is_some()
            && self.pid.is_some()
            && self.eyr.is_some()
            && self.hcl.is_some()
            && self.byr.is_some()
            && self.iyr.is_some()
            && self.hgt.is_some()
    }

    fn is_valid_b(&self) -> bool {
        return self.is_valid()
            && year_in_range(&self.byr, 1920, 2002)
            && year_in_range(&self.iyr, 2010, 2020)
            && year_in_range(&self.eyr, 2020, 2030)
            && int_of_length(&self.pid, 9)
            && hex_code(&self.hcl)
            && str_in_set(
                &self.ecl,
                vec!["amb", "blu", "brn", "gry", "grn", "hzl", "oth"],
            )
            && valid_hgt(&self.hgt);
    }
}

impl FromStr for Passport {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let m: HashMap<_, _> = s
            // Replace all linebreaks with whitespaces
            .replace('\n', " ")
            // Split all fields
            .split(' ')
            // Parse the fields into a dictionary
            .map(|t| t.split(':'))
            .map(|mut kv| {
                (
                    kv.next().unwrap_or("").into(),
                    kv.next().unwrap_or("").into(),
                )
            })
            .collect::<HashMap<String, String>>();
        Ok(Passport {
            byr: m.get("byr").cloned(),
            cid: m.get("cid").cloned(),
            ecl: m.get("ecl").cloned(),
            eyr: m.get("eyr").cloned(),
            hcl: m.get("hcl").cloned(),
            hgt: m.get("hgt").cloned(),
            iyr: m.get("iyr").cloned(),
            pid: m.get("pid").cloned(),
        })
    }
}

fn parse_input(input: &str) -> Context {
    Context {
        passports: input
            .split("\n\n")
            .map(|s| Passport::from_str(s).expect("couldn't parse passport"))
            .collect(),
    }
}

fn solve_a(input: &str) -> usize {
    parse_input(input)
        .passports
        .iter()
        .filter(|p| p.is_valid())
        .count()
}

fn solve_b(input: &str) -> usize {
    parse_input(input)
        .passports
        .iter()
        .filter(|p| p.is_valid_b())
        .count()
}

fn main() {
    println!("Day04a: {}", solve_a(load_input(4).as_str()));
    println!("Day04b: {}", solve_b(load_input(4).as_str()));
}

#[test]
fn test() {
    let sample_input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\nbyr:1937 iyr:2017 cid:147 hgt:183cm\n\niyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\nhcl:#cfa07d byr:1929\n\nhcl:#ae17e1 iyr:2013\neyr:2024\necl:brn pid:760753108 byr:1931\nhgt:179cm\n\nhcl:#cfa07d eyr:2025 pid:166559648\niyr:2011 ecl:brn hgt:59in";
    let result_a = solve_a(sample_input);
    assert_eq!(result_a, 2);
    let sample_input_b = "eyr:1972 cid:100\nhcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926\n\niyr:2019\nhcl:#602927 eyr:1967 hgt:170cm\necl:grn pid:012533040 byr:1946\n\nhcl:dab227 iyr:2012\necl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277\n\nhgt:59cm ecl:zzz\neyr:2038 hcl:74454a iyr:2023\npid:3556412378 byr:2007\n\npid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980\nhcl:#623a2f\n\neyr:2029 ecl:blu cid:129 byr:1989\niyr:2014 pid:896056539 hcl:#a97842 hgt:165cm\n\nhcl:#888785\nhgt:164cm byr:2001 iyr:2015 cid:88\npid:545766238 ecl:hzl\neyr:2022\n\niyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719\n";
    let result_b = solve_b(sample_input_b);
    assert_eq!(result_b, 4);
    assert_eq!(str_in_set(&Some("a".to_string()), vec!["a", "b"]), true);
    assert_eq!(str_in_set(&Some("c".to_string()), vec!["a", "b"]), false);
    assert_eq!(hex_code(&Some("#abcdefg".to_string())), false);
    assert_eq!(hex_code(&Some("#abcdef0".to_string())), false);
    assert_eq!(int_of_length(&Some("0123456789".to_string()), 9), false);
    assert_eq!(int_of_length(&Some("123456789".to_string()), 9), true);
    assert_eq!(int_of_length(&Some("000000001".to_string()), 9), true);
    assert_eq!(int_of_length(&Some("000000000".to_string()), 9), true);
    assert_eq!(int_of_length(&Some("00000000".to_string()), 9), false);
    assert_eq!(year_in_range(&Some("2002".to_string()), 2001, 2004), true);
    assert_eq!(year_in_range(&Some("2002".to_string()), 2002, 2004), true);
    assert_eq!(year_in_range(&Some("2002".to_string()), 2003, 2004), false);
    assert_eq!(year_in_range(&Some("2002".to_string()), 2001, 2002), true);
    assert_eq!(year_in_range(&Some("2002".to_string()), 1900, 2001), false);
    assert_eq!(valid_hgt(&Some("60in".to_string())), true);
    assert_eq!(valid_hgt(&Some("190cm".to_string())), true);
    assert_eq!(valid_hgt(&Some("190in".to_string())), false);
    assert_eq!(valid_hgt(&Some("190".to_string())), false);
}
