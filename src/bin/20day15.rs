fn parse(input: &str) -> Vec<i64> {
    input
        .split(',')
        .map(|s| s.parse::<i64>().expect("failed to parse i64"))
        .collect()
}

fn populate(list: &Vec<i64>, map: &mut Vec<i64>) -> i64 {
    let mut number = 0;
    list.iter().enumerate().for_each(|(idx, x)| {
        map[*x as usize] = idx as i64 + 1;
        number = *x;
    });
    number
}

fn solve(input: &str, count: usize) -> i64 {
    let list = parse(input);
    let mut map: Vec<i64> = vec![0; count];
    let mut number = populate(&list, &mut map);
    for idx in (list.len() + 1)..=count {
        let old_number = number;
        number = match map[number as usize] {
            0 => 0,
            y => idx as i64 - 1 - y,
        };
        map[old_number as usize] = idx as i64 - 1;
    }
    number
}

fn solve_a(input: &str) -> i64 {
    solve(input, 2020)
}

fn solve_b(input: &str) -> i64 {
    solve(input, 30000000)
}

fn main() {
    let day = 15;
    println!("Day{:02}a: {}", day, solve_a("2,1,10,11,0,6"));
    println!("Day{:02}b: {}", day, solve_b("2,1,10,11,0,6"));
}

#[test]
fn test() {
    let input1 = "0,3,6";
    assert_eq!(solve_a(input1), 436);
    assert_eq!(solve_a("1,3,2"), 1);
    assert_eq!(solve_a("2,1,3"), 10);
    assert_eq!(solve_a("1,2,3"), 27);
    assert_eq!(solve_a("2,3,1"), 78);
    assert_eq!(solve_a("3,2,1"), 438);
    assert_eq!(solve_a("3,1,2"), 1836);
    assert_eq!(solve_b("0,3,6"), 175594);
    // Running all tests is too slow
    // assert_eq!(solve_b("1,3,2"), 2578);
    // assert_eq!(solve_b("2,1,3"), 3544142);
    // assert_eq!(solve_b("1,2,3"), 261214);
    // assert_eq!(solve_b("2,3,1"), 6895259);
    // assert_eq!(solve_b("3,2,1"), 18);
    // assert_eq!(solve_b("3,1,2"), 362);
}
