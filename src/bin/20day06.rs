use advent_of_code_2020::load_input;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::str::FromStr;
use std::string::ParseError;

#[derive(Debug)]
struct Context {
    groups: Vec<Group>,
}

#[derive(Debug)]
struct Group {
    persons: Vec<HashSet<char>>,
}

impl Context {
    fn yes_count(&self) -> usize {
        self.groups.iter().map(|g| g.yes_count()).sum()
    }

    fn yes_join(&self) -> usize {
        self.groups.iter().map(|g| g.yes_join()).sum()
    }
}

impl Group {
    fn yes_count(&self) -> usize {
        let total: HashSet<char> = self
            .persons
            .iter()
            .fold(HashSet::new(), |h, x| h.union(x).copied().collect());
        total.len()
    }

    fn yes_join(&self) -> usize {
        let total: HashSet<char> = self.persons.iter().fold(
            self.persons.first().expect("group without persons").clone(),
            |h, x| h.intersection(x).copied().collect(),
        );
        total.len()
    }
}

impl FromStr for Group {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let persons: Vec<HashSet<char>> =
            s.lines().map(|p| HashSet::from_iter(p.chars())).collect();
        Ok(Group { persons })
    }
}

impl FromStr for Context {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let groups = s
            .split("\n\n")
            .map(|s| Group::from_str(s).expect("Failed to parse group"))
            .collect();
        Ok(Context { groups })
    }
}

fn solve_a(input: &str) -> usize {
    Context::from_str(input)
        .expect("Failed to parse input")
        .yes_count()
}

fn solve_b(input: &str) -> usize {
    Context::from_str(input)
        .expect("Failed to parse input")
        .yes_join()
}

fn main() {
    println!("Day06a: {}", solve_a(load_input(6).as_str()));
    println!("Day06b: {}", solve_b(load_input(6).as_str()));
}

#[test]
fn test() {
    let input = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb\n";
    assert_eq!(solve_a(input), 11);
    assert_eq!(solve_b(input), 6);
}
