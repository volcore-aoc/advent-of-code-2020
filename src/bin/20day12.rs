use advent_of_code_2020::load_input;
use advent_of_code_2020::math::Vec2i64;
use std::str::FromStr;
use std::string::ParseError;

#[derive(Debug, Clone)]
struct Context {
    instructions: Vec<Instruction>,
    xy: Vec2i64,
    dir: i64,
    wp: Vec2i64,
}

#[derive(Debug, Clone)]
enum Instruction {
    North(i64),
    South(i64),
    West(i64),
    East(i64),
    Left(i64),
    Right(i64),
    Forward(i64),
}

impl FromStr for Instruction {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let d = s[1..].parse::<i64>().unwrap();
        match s.chars().nth(0) {
            Some('F') => Ok(Instruction::Forward(d)),
            Some('N') => Ok(Instruction::North(d)),
            Some('W') => Ok(Instruction::West(d)),
            Some('E') => Ok(Instruction::East(d)),
            Some('S') => Ok(Instruction::South(d)),
            Some('L') => Ok(Instruction::Left(d)),
            Some('R') => Ok(Instruction::Right(d)),
            _ => Err("Unknown instruction".to_string()),
        }
    }
}

impl Context {
    pub fn run(&mut self) -> &mut Self {
        const DIRECTIONS: [Vec2i64; 4] = [
            Vec2i64 { x: 1, y: 0 },
            Vec2i64 { x: 0, y: 1 },
            Vec2i64 { x: -1, y: 0 },
            Vec2i64 { x: 0, y: -1 },
        ];
        self.instructions
            .clone()
            .iter()
            .for_each(|inst| match inst {
                Instruction::West(d) => self.xy.x -= d,
                Instruction::East(d) => self.xy.x += d,
                Instruction::North(d) => self.xy.y += d,
                Instruction::South(d) => self.xy.y -= d,
                Instruction::Left(d) => self.dir = (self.dir + d / 90) % 4,
                Instruction::Right(d) => self.dir = (self.dir - d / 90 + 4) % 4,
                Instruction::Forward(d) => self.xy.muladd(d, &DIRECTIONS[(self.dir % 4) as usize]),
            });
        self
    }

    pub fn run_b(&mut self) -> &mut Self {
        self.instructions
            .clone()
            .iter()
            .for_each(|inst| match inst {
                Instruction::West(d) => self.wp.x -= d,
                Instruction::East(d) => self.wp.x += d,
                Instruction::North(d) => self.wp.y += d,
                Instruction::South(d) => self.wp.y -= d,
                Instruction::Left(d) => self.wp.rotate(-d / 90),
                Instruction::Right(d) => self.wp.rotate(d / 90),
                Instruction::Forward(d) => self.xy.muladd(d, &self.wp),
            });
        self
    }

    pub fn distance(&self) -> i64 {
        self.xy.manhattan()
    }
}

impl FromStr for Context {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Context {
            instructions: s
                .lines()
                .map(Instruction::from_str)
                .filter(|x| x.is_ok())
                .map(|x| x.unwrap())
                .collect(),
            xy: Vec2i64 { x: 0, y: 0 },
            dir: 0,
            wp: Vec2i64 { x: 10, y: 1 },
        })
    }
}

fn solve_a(input: &str) -> i64 {
    Context::from_str(input)
        .expect("failed to parse")
        .run()
        .distance()
}

fn solve_b(input: &str) -> i64 {
    Context::from_str(input)
        .expect("failed to parse")
        .run_b()
        .distance()
}

fn main() {
    let day = 12;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input1 = "F10\nN3\nF7\nR90\nF11";
    assert_eq!(solve_a(input1), 25);
    assert_eq!(solve_b(input1), 286);
}
