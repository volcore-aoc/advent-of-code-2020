use advent_of_code_2020::load_input;
use std::collections::{HashMap, HashSet};

#[derive(Debug)]
struct Context {
    rules: HashMap<usize, Rule>,
    strings: Vec<String>,
}

#[derive(Debug)]
enum Rule {
    List(Vec<usize>),
    Split(Vec<usize>, Vec<usize>),
    Char(char),
}

impl Rule {
    fn parse(s: &str) -> Rule {
        if s.contains("\"") {
            Rule::Char(s.chars().nth(1).unwrap())
        } else if s.contains("|") {
            let parts: Vec<&str> = s.split(" | ").collect();
            Rule::Split(
                parts[0]
                    .split(' ')
                    .flat_map(|r| r.parse::<usize>())
                    .collect(),
                parts[1]
                    .split(' ')
                    .flat_map(|r| r.parse::<usize>())
                    .collect(),
            )
        } else {
            Rule::List(s.split(' ').flat_map(|r| r.parse::<usize>()).collect())
        }
    }
}

impl Context {
    fn parse(s: &str) -> Context {
        let rules = s
            .split("\n\n")
            .nth(0)
            .unwrap()
            .lines()
            .map(|r| {
                let parts: Vec<&str> = r.split(": ").collect();
                (parts[0].parse::<usize>().unwrap(), Rule::parse(parts[1]))
            })
            .collect();
        let strings = s
            .split("\n\n")
            .nth(1)
            .unwrap()
            .lines()
            .map(|s| s.to_string())
            .collect();
        Context { rules, strings }
    }

    fn match_sublist(&self, l: &Vec<usize>, inputs: &HashSet<String>) -> HashSet<String> {
        l.iter().fold(inputs.clone(), |set, sub_rule| {
            self.match_remainder(sub_rule, &set)
        })
    }

    fn match_remainder(&self, rule_id: &usize, inputs: &HashSet<String>) -> HashSet<String> {
        if inputs.len() == 0 {
            return HashSet::new();
        }
        let rule = self.rules.get(&rule_id).unwrap();
        let result: HashSet<String> = match rule {
            // In case of a single char, find all inputs that start with that char, and return all
            // the postfixes without that char.
            Rule::Char(c) => inputs
                .iter()
                .filter(|&s| s.starts_with(&c.to_string()))
                .map(|s| s[1..].to_string())
                .collect(),
            // If this is a list, run through each step and collect all possible matches
            Rule::List(l) => self.match_sublist(l, inputs),
            // If this is a split, try each side and combine both results
            Rule::Split(l, r) => {
                let left_result = self.match_sublist(l, inputs);
                let right_result = self.match_sublist(r, inputs);
                left_result.union(&right_result).cloned().collect()
            }
        };
        result
    }

    fn matches(&self, s: &str, rule: usize) -> bool {
        let set = [s.to_string()].iter().cloned().collect();
        let result = self.match_remainder(&rule, &set);
        // Check if we have a perfect match (in which case the remainder should include the empty
        // string
        result.contains("")
    }

    fn count_matching(&self) -> usize {
        self.strings.iter().filter(|s| self.matches(s, 0)).count()
    }
}

fn solve_a(s: &str) -> usize {
    let ctx = Context::parse(s);
    ctx.count_matching()
}

fn solve_b(s: &str) -> usize {
    let mut ctx = Context::parse(s);
    ctx.rules.insert(8, Rule::Split(vec![42], vec![42, 8]));
    ctx.rules
        .insert(11, Rule::Split(vec![42, 31], vec![42, 11, 31]));
    ctx.count_matching()
}

fn main() {
    let day = 19;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input = "0: 4 1 5\n1: 2 3 | 3 2\n2: 4 4 | 5 5\n3: 4 5 | 5 4\n4: \"a\"\n5: \"b\"\n\nababbb\nbababa\nabbbab\naaabbb\naaaabbb";
    assert_eq!(solve_a(input), 2);
    let input_b = "42: 9 14 | 10 1\n9: 14 27 | 1 26\n10: 23 14 | 28 1\n1: \"a\"\n11: 42 31\n5: 1 14 | 15 1\n19: 14 1 | 14 14\n12: 24 14 | 19 1\n16: 15 1 | 14 14\n31: 14 17 | 1 13\n6: 14 14 | 1 14\n2: 1 24 | 14 4\n0: 8 11\n13: 14 3 | 1 12\n15: 1 | 14\n17: 14 2 | 1 7\n23: 25 1 | 22 14\n28: 16 1\n4: 1 1\n20: 14 14 | 1 15\n3: 5 14 | 16 1\n27: 1 6 | 14 18\n14: \"b\"\n21: 14 1 | 1 14\n25: 1 1 | 1 14\n22: 14 14\n8: 42\n26: 14 22 | 1 20\n18: 15 15\n7: 14 5 | 1 21\n24: 14 1\n\nabbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa\nbbabbbbaabaabba\nbabbbbaabbbbbabbbbbbaabaaabaaa\naaabbbbbbaaaabaababaabababbabaaabbababababaaa\nbbbbbbbaaaabbbbaaabbabaaa\nbbbababbbbaaaaaaaabbababaaababaabab\nababaaaaaabaaab\nababaaaaabbbaba\nbaabbaaaabbaaaababbaababb\nabbbbabbbbaaaababbbbbbaaaababb\naaaaabbaabaaaaababaa\naaaabbaaaabbaaa\naaaabbaabbaaaaaaabbbabbbaaabbaabaaa\nbabaaabbbaaabaababbaabababaaab\naabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba";
    assert_eq!(solve_b(input_b), 12);
}
