use advent_of_code_2020::load_input;
use std::collections::HashMap;

#[derive(Debug)]
enum Instruction {
    SetMask(i64, i64),
    WriteMem(i64, i64),
    Noop,
}

fn parse(input: &str) -> Vec<Instruction> {
    input
        .lines()
        .map(|s| {
            let parts: Vec<&str> = s.split(" = ").collect();
            if parts[0] == "mask" {
                let mask = parts[1]
                    .chars()
                    .enumerate()
                    .map(|(idx, c)| match c {
                        'X' => (idx, 0i64),
                        _ => (idx, 1i64),
                    })
                    .fold(0i64, |mask, (idx, x)| mask | (x << (35 - idx)));
                let value = parts[1]
                    .chars()
                    .enumerate()
                    .map(|(idx, c)| match c {
                        '1' => (idx, 1i64),
                        _ => (idx, 0i64),
                    })
                    .fold(0i64, |value, (idx, x)| value | (x << (35 - idx)));
                Instruction::SetMask(mask, value)
            } else if parts[0].starts_with("mem") {
                Instruction::WriteMem(
                    parts[0][4..parts[0].len() - 1].parse::<i64>().unwrap(),
                    parts[1].parse::<i64>().unwrap(),
                )
            } else {
                Instruction::Noop
            }
        })
        .collect()
}

fn solve_a(input: &str) -> i64 {
    let instructions = parse(input);
    let mut mem: HashMap<i64, i64> = HashMap::new();
    let mut mask = 0i64;
    let mut maskvalue = 0i64;
    for inst in instructions {
        match inst {
            Instruction::SetMask(new_mask, new_value) => {
                mask = new_mask;
                maskvalue = new_value;
            }
            Instruction::WriteMem(offset, value) => {
                mem.insert(offset, value & !mask | maskvalue);
            }
            _ => {}
        }
    }
    mem.values().sum()
}

fn solve_b(input: &str) -> i64 {
    let instructions = parse(input);
    let mut mem: HashMap<i64, i64> = HashMap::new();
    let mut maskvalue = 0i64;
    let mut maskbits: Vec<i64> = Vec::new();
    for inst in instructions {
        match inst {
            Instruction::SetMask(mask, new_value) => {
                maskvalue = new_value;
                // Count and detect the bits
                maskbits.clear();
                for i in 0..36 {
                    if ((!mask) & (1 << i)) > 0 {
                        maskbits.push(i);
                    }
                }
            }
            Instruction::WriteMem(offset, value) => {
                let base = offset | maskvalue;
                // count the bits that are on
                let count = maskbits.len();
                let permuations = 1 << count;
                for i in 0..permuations {
                    let mut real_offset = base;
                    for (bit_idx, bit_offset) in maskbits.iter().enumerate() {
                        if i & (1 << bit_idx) > 0 {
                            // set the bit
                            real_offset |= 1 << bit_offset;
                        } else {
                            // remove the bit
                            real_offset &= !(1 << bit_offset);
                        }
                    }
                    mem.insert(real_offset, value);
                }
            }
            _ => {}
        }
    }
    mem.values().sum()
}

fn main() {
    let day = 14;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input1 =
        "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\nmem[8] = 11\nmem[7] = 101\nmem[8] = 0";
    assert_eq!(solve_a(input1), 165);
    let input2 = "mask = 000000000000000000000000000000X1001X\nmem[42] = 100\nmask = 00000000000000000000000000000000X0XX\nmem[26] = 1";
    assert_eq!(solve_b(input2), 208);
}
