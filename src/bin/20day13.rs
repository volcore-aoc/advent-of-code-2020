use advent_of_code_2020::load_input;

fn solve_a(input: &str) -> i64 {
    let lines: Vec<i64> = input
        .lines()
        .nth(1)
        .expect("failed to parse")
        .split(',')
        .filter(|&x| x != "x")
        .map(|x| x.parse::<i64>().expect("invalid bus number"))
        .collect();
    let departure = input
        .lines()
        .nth(0)
        .map(|s| s.parse::<i64>().expect("invalid departure"))
        .expect("invalid departure");
    let mut min = i64::max_value();
    let mut min_line = 0;
    for x in lines {
        let y = departure % x;
        let dt = x - y;
        if dt < min {
            min = dt;
            min_line = x;
        }
    }
    min_line * min
}

fn solve_b(input: &str) -> i64 {
    let list: Vec<(i64, i64)> = input
        .lines()
        .nth(1)
        .expect("failed to parse")
        .split(',')
        .enumerate()
        .filter(|&(_, x)| x != "x")
        .map(|(i, x)| (i as i64, x.parse::<i64>().expect("invalid bus number")))
        .collect();
    let mut timestamp = 0;
    let mut stride = list[0].1;
    for (idx, bus) in list.iter().skip(1) {
        while (timestamp + idx) % bus != 0 {
            timestamp += stride;
        }
        stride *= bus;
    }
    timestamp
}

fn main() {
    let day = 13;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input1 = "939\n7,13,x,x,59,x,31,19";
    assert_eq!(solve_a(input1), 295);
    assert_eq!(solve_b(input1), 1068781);
    let input2 = "939\n17,x,13,19";
    assert_eq!(solve_b(input2), 3417);
    let input3 = "939\n67,7,59,61";
    assert_eq!(solve_b(input3), 754018);
    let input4 = "939\n67,x,7,59,61";
    assert_eq!(solve_b(input4), 779210);
    let input5 = "939\n67,7,x,59,61";
    assert_eq!(solve_b(input5), 1261476);
    let input6 = "939\n1789,37,47,1889";
    assert_eq!(solve_b(input6), 1202161486);
}
