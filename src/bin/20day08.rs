use std::str::FromStr;

use advent_of_code_2020::load_input;
use advent_of_code_2020::vm::{Instruction, VM};

fn solve_a(input: &str) -> i64 {
    let mut vm = VM::from_str(input).expect("failed to load code");
    vm.does_terminate();
    vm.acc
}

fn solve_b(input: &str) -> i64 {
    let mut vm = VM::from_str(input).expect("failed to load code");
    let mut idx = 0;
    loop {
        // Modify the vm for the given instruction
        let inst = vm.get_code(idx).clone();
        match inst {
            Instruction::Jmp(x) => vm.set_code(idx, &Instruction::Nop(x.clone())),
            Instruction::Nop(x) => vm.set_code(idx, &Instruction::Jmp(x.clone())),
            _ => {}
        }
        if vm.does_terminate() {
            return vm.acc;
        }
        // Put the original instruction back in place
        vm.set_code(idx, &inst);
        vm.reset();
        idx += 1
    }
}

fn main() {
    let day = 8;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input = "nop +0\nacc +1\njmp +4\nacc +3\njmp -3\nacc -99\nacc +1\njmp -4\nacc +6\n\n";
    assert_eq!(solve_a(input), 5);
    assert_eq!(solve_b(input), 8);
}
