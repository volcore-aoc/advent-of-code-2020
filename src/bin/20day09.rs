use advent_of_code_2020::{load_input, parse_int_list};
use std::collections::VecDeque;

fn is_sum_of_two(v: i64, l: &[i64]) -> bool {
    for (ix, x) in l.iter().enumerate() {
        // Only check the right side of x
        for y in l.split_at(ix).1 {
            if x + y == v {
                return true;
            }
        }
    }
    false
}

fn solve_a(s: &str, plen: usize) -> i64 {
    let mut list = VecDeque::from(parse_int_list(s));
    let mut buf: VecDeque<i64> = VecDeque::with_capacity(plen);
    // Put into the buf
    for _ in 0..plen {
        buf.push_back(list.pop_front().unwrap());
    }
    // Now process the each item
    while !list.is_empty() {
        let v = list.pop_front().unwrap();
        buf.make_contiguous();
        if !is_sum_of_two(v, buf.as_slices().0) {
            return v;
        }
        buf.pop_front();
        buf.push_back(v);
    }
    0
}

fn solve_b(s: &str, plen: usize) -> i64 {
    let p = solve_a(s, plen);
    // Build an SAT
    let list = parse_int_list(s);
    let mut sat: Vec<i64> = Vec::new();
    let mut sat_value = 0i64;
    for x in list.iter() {
        sat_value += x;
        sat.push(sat_value);
    }
    // Find
    for (ix, x) in sat.iter().enumerate() {
        for (iy2, y) in sat[ix..].iter().enumerate() {
            if y - x == p {
                let iy = ix + iy2;
                let max = list[ix + 1..iy + 1].iter().max().unwrap();
                let min = list[ix + 1..iy + 1].iter().min().unwrap();
                return min + max;
            }
        }
    }
    0
}

fn main() {
    let day = 9;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str(), 25));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str(), 25));
}

#[test]
fn test() {
    let input =
        "35\n20\n15\n25\n47\n40\n62\n55\n65\n95\n102\n117\n150\n182\n127\n219\n299\n277\n309\n576";
    assert_eq!(solve_a(input, 5), 127);
    assert_eq!(solve_b(input, 5), 62);
}
