use advent_of_code_2020::math::Vec2i64;
use advent_of_code_2020::{load_input, parse_int_list_ext};
use std::collections::HashSet;
use std::str::FromStr;

#[derive(Debug)]
struct Context {
    rules: Vec<Rule>,
    my_ticket: Vec<i64>,
    nearby_tickets: Vec<Vec<i64>>,
}

#[derive(Debug)]
struct Rule {
    name: String,
    ranges: Vec<Vec2i64>,
}

impl FromStr for Rule {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.splitn(2, ": ").collect();
        let ranges = parts[1].split(" or ").map(Vec2i64::from_range).collect();
        Ok(Rule {
            name: parts[0].to_string(),
            ranges,
        })
    }
}

impl Rule {
    pub fn valid_range(&self, x: i64) -> bool {
        self.ranges.iter().any(|&r| x >= r.x && x <= r.y)
    }
}

impl FromStr for Context {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split("\n\n").collect();
        let rules = parts[0]
            .lines()
            .map(Rule::from_str)
            .map(|x| x.unwrap())
            .collect();
        let my_ticket = parse_int_list_ext(parts[1].lines().nth(1).unwrap(), ',');
        let nearby_tickets = parts[2]
            .lines()
            .skip(1)
            .map(|s| parse_int_list_ext(s, ','))
            .collect();
        Ok(Context {
            rules,
            my_ticket,
            nearby_tickets,
        })
    }
}

fn solve_a(input: &str) -> i64 {
    let ctx = Context::from_str(input).unwrap();
    let mut sum = 0i64;
    for ticket in ctx.nearby_tickets {
        for x in ticket {
            if !ctx.rules.iter().any(|r| r.valid_range(x)) {
                sum += x;
            }
        }
    }
    sum
}

fn solve_b(input: &str) -> i64 {
    let ctx = Context::from_str(input).unwrap();
    let valid_tickets: Vec<&Vec<i64>> = ctx
        .nearby_tickets
        .iter()
        .filter(|&ticket| {
            ticket
                .iter()
                .all(|&x| ctx.rules.iter().any(|r| r.valid_range(x)))
        })
        .collect();
    // TODO(VS): find mapping
    let count = valid_tickets[0].len();
    let mut remaining: HashSet<usize> = (0..count).collect();
    let mut total = 1;
    while remaining.len() > 0 {
        for rule in ctx.rules.iter() {
            let matches: Vec<usize> = remaining
                .to_owned()
                .into_iter()
                .filter(|&column| {
                    valid_tickets
                        .iter()
                        .all(|&ticket| rule.valid_range(ticket[column]))
                })
                .collect();
            if matches.len() == 1 {
                remaining.remove(&matches[0]);
                // If it's a departure rule add it to the total
                if rule.name.starts_with("departure") {
                    total *= ctx.my_ticket[matches[0]];
                }
                break;
            }
        }
    }
    total
}

fn main() {
    let day = 16;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input1 = "class: 1-3 or 5-7\nrow: 6-11 or 33-44\nseat: 13-40 or 45-50\n\nyour ticket:\n7,1,14\n\nnearby tickets:\n7,3,47\n40,4,50\n55,2,20\n38,6,12";
    assert_eq!(solve_a(input1), 71);
    let input2 = "departure class: 0-1 or 4-19\nrow: 0-5 or 8-19\nseat: 0-13 or 16-19\n\nyour ticket:\n11,12,13\n\nnearby tickets:\n3,9,18\n15,1,5\n5,14,9\n5,1411,9";
    assert_eq!(solve_b(input2), 12);
}
