use advent_of_code_2020::{load_input, parse_int_list};
use std::collections::HashMap;

fn solve_a(input: &str) -> i64 {
    let mut list = parse_int_list(input);
    list.sort();
    let (_, ones, threes) = list
        .iter()
        .fold((0, 0, 1), |(p, ones, threes), x| match x - p {
            1 => (*x, ones + 1, threes),
            3 => (*x, ones, threes + 1),
            _ => (*x, ones, threes),
        });
    ones * threes
}

fn count_arrangements(list: &[i64], cache: &mut HashMap<i64, i64>) -> i64 {
    if list.len() == 1 {
        1
    } else {
        let first = list.first().unwrap();
        // Check the cache
        if cache.contains_key(first) {
            return cache[first];
        }
        // Find all prefixes that can be skipped
        let mut count = 0i64;
        for idx in 1..list.len() {
            // Check if this is a potential arrangement. if the distance is too great, no further
            // arrangements can be made from this sub-list, so abort
            if list[idx] - first > 3 {
                break;
            }
            // Count all arrangements for this sub-list
            count += count_arrangements(&list[idx..], cache);
        }
        cache.insert(*first, count);
        count
    }
}

fn solve_b(input: &str) -> i64 {
    let mut list = parse_int_list(input);
    list.push(0);
    list.push(list.iter().max().unwrap() + 3);
    list.sort();
    let mut cache = HashMap::new();
    count_arrangements(list.as_slice(), &mut cache)
}

fn main() {
    let day = 10;
    println!("Day{:02}a: {}", day, solve_a(load_input(day).as_str()));
    println!("Day{:02}b: {}", day, solve_b(load_input(day).as_str()));
}

#[test]
fn test() {
    let input1 = "16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4";
    let input2 = "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3";
    assert_eq!(solve_a(input1), 7 * 5);
    assert_eq!(solve_a(input2), 22 * 10);
    assert_eq!(solve_b(input1), 8);
    assert_eq!(solve_b(input2), 19208);
}
