use advent_of_code_2020::{load_input, parse_int_list};

fn main() {
    println!("Day01a: {}", day01a(load_input(1).as_str()));
    println!("Day01b: {}", day01b(load_input(1).as_str()));
}

fn day01a(input: &str) -> i64 {
    let list = parse_int_list(input);
    for (x_idx, x) in list.iter().enumerate() {
        // Fetch the right side of the list, as we only need to check those values
        let (_left, right) = list.split_at(x_idx);
        for (_y_idx, y) in right.iter().enumerate() {
            // If we have found the result, immediately return
            if x + y == 2020 {
                return x * y;
            }
        }
    }
    // Nothing found, so return an invalid value
    -1
}

fn day01b(input: &str) -> i64 {
    let list = parse_int_list(input);
    for (x_idx, x) in list.iter().enumerate() {
        // Fetch the right side of the list, as we only need to check those values
        let (_left, right) = list.split_at(x_idx);
        for (y_idx, y) in right.iter().enumerate() {
            // Do the same thing again
            let (_left2, right_z) = right.split_at(y_idx);
            for (_z_idx, z) in right_z.iter().enumerate() {
                // If we have found the result, immediately return
                if x + y + z == 2020 {
                    return x * y * z;
                }
            }
        }
    }
    // Nothing found, so return an invalid value
    -1
}

#[test]
fn test20day01() {
    let sample_input = "1721\n979\n366\n299\n675\n1456";
    let result_a = day01a(sample_input);
    assert_eq!(result_a, 514579);
    let result_b = day01b(sample_input);
    assert_eq!(result_b, 241861950);
}
