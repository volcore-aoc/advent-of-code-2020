use advent_of_code_2020::load_input;

use advent_of_code_2020::math::Vec2i64;

fn main() {
    println!("Day03a: {}", solve_a(load_input(3).as_str()));
    println!("Day03b: {}", solve_b(load_input(3).as_str()));
}

#[derive(Debug)]
struct Context {
    grid: Vec<String>,
}

impl Context {
    fn walk_slope(&self, dir: &Vec2i64) -> i64 {
        let mut xy = Vec2i64 { x: 0, y: 0 };
        let height = self.grid.len() as i64;
        let width = self.grid[0].len() as i64;
        let mut trees = 0 as i64;
        loop {
            xy.x = (xy.x + dir.x) % width;
            xy.y += dir.y;
            if xy.y >= height {
                break;
            }
            // Check if this location is a tree
            if self.at(&xy) == '#' {
                trees += 1
            }
        }
        trees
    }

    fn at(&self, xy: &Vec2i64) -> char {
        self.grid
            .get(xy.y as usize)
            .expect("Out of bounds read on grid")
            .chars()
            .nth(xy.x as usize)
            .expect("Out of bounds read on grid row")
    }
}

fn parse_input(input: &str) -> Context {
    Context {
        grid: input.lines().map(String::from).collect(),
    }
}

fn solve_a(input: &str) -> i64 {
    parse_input(input).walk_slope(&Vec2i64 { x: 3, y: 1 })
}

fn solve_b(input: &str) -> i64 {
    let ctx = parse_input(input);
    ctx.walk_slope(&Vec2i64 { x: 1, y: 1 })
        * ctx.walk_slope(&Vec2i64 { x: 3, y: 1 })
        * ctx.walk_slope(&Vec2i64 { x: 5, y: 1 })
        * ctx.walk_slope(&Vec2i64 { x: 7, y: 1 })
        * ctx.walk_slope(&Vec2i64 { x: 1, y: 2 })
}

#[test]
fn test() {
    let sample_input = "..##.......\n#...#...#..\n.#....#..#.\n..#.#...#.#\n.#...##..#.\n..#.##.....\n.#.#.#....#\n.#........#\n#.##...#...\n#...##....#\n.#..#...#.#";
    let result_a = solve_a(sample_input);
    assert_eq!(result_a, 7);
    let result_b = solve_b(sample_input);
    assert_eq!(result_b, 336);
}
