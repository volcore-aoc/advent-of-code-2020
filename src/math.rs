use std::cmp::{max, min};

#[derive(Debug, Clone, Copy)]
pub struct Vec2i64 {
    pub x: i64,
    pub y: i64,
}

impl Vec2i64 {
    pub fn zero() -> Vec2i64 {
        Vec2i64 { x: 0, y: 0 }
    }

    pub fn empty_bounds() -> Vec2i64 {
        Vec2i64 {
            x: i64::max_value(),
            y: i64::min_value(),
        }
    }

    pub fn add(&mut self, o: &Vec2i64) {
        self.x += o.x;
        self.y += o.y;
    }

    pub fn set(&mut self, o: &Vec2i64) {
        self.x = o.x;
        self.y = o.y;
    }

    pub fn muladd(&mut self, d: &i64, o: &Vec2i64) {
        self.x += o.x * d;
        self.y += o.y * d;
    }

    pub fn from_range(s: &str) -> Vec2i64 {
        let parts: Vec<i64> = s
            .splitn(2, "-")
            .map(|s| s.parse::<i64>().expect("range part not a number"))
            .collect();
        Vec2i64 {
            x: parts[0],
            y: parts[1],
        }
    }

    pub fn inside_bounds(&self, min: &Vec2i64, max: &Vec2i64) -> bool {
        return self.x >= min.x && self.x <= max.x && self.y >= min.y && self.y <= max.y;
    }

    pub fn extend_bounds(&self, bounds: &Vec2i64) -> Vec2i64 {
        Vec2i64 {
            x: min(bounds.x, self.x),
            y: max(bounds.y, self.y),
        }
    }

    pub fn manhattan(&self) -> i64 {
        self.x.abs() + self.y.abs()
    }

    // Rotate the point around the center. Steps is the number of 90 degree turns
    pub fn rotate(&mut self, steps: i64) {
        match steps % 4 {
            1 | -3 => self.set(&Vec2i64 {
                x: self.y,
                y: -self.x,
            }),
            2 | -2 => self.set(&Vec2i64 {
                x: -self.x,
                y: -self.y,
            }),
            3 | -1 => self.set(&Vec2i64 {
                x: -self.y,
                y: self.x,
            }),
            _ => {}
        }
    }
}

pub fn gcd(a: i64, b: i64) -> i64 {
    match b {
        0 => a,
        _ => gcd(b, a % b),
    }
}

pub fn lcm(a: i64, b: i64) -> i64 {
    a * b / gcd(a, b)
}
