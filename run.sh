#!/bin/bash

# Pass in the day number as an arg

set -e

cargo build --release
time ./target/release/20day$1
